FROM python:3.12-slim-bookworm

RUN apt update && \
    apt install -y git && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --upgrade pip && \
    pip install pipenv
